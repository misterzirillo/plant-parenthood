(ns pp.device.metrics
  (:require
    [cljs-http.client :as http]
    [cljs.spec.alpha :as s]
    [cljs.core.async :as a]
    [clojure.string :refer [replace]]
    [pp.device.util.time :as time]
    [pp.device.util.influxdb :as influxdb]
    [taoensso.timbre :as log]))

(defn validate-metric [m]
  (let [valid (s/valid? ::influxdb/point m)]
    (when-not valid
      (log/warn "invalid metric" (s/explain-data ::influxdb/point m)))
    valid))

;; shim to make cljs-http work in nodejs
;; https://github.com/r0man/cljs-http/issues/94
(set! js/XMLHttpRequest (js/require "xhr2"))

(def ^:private zlib (js/require "zlib"))

(defn- gzip [s]
  (let [p (a/promise-chan)]
    (.gzip zlib s
           (fn [e d]
             (if-not e
               (a/put! p d)
               (do (log/error e)
                   (a/close! p)))))
    p))

(defn post-metrics [{:keys [host apikey org bucket]} metrics]
  (let [now   (-> (time/now) (time/unix-seconds))
        lines (transduce
                (comp (map #(assoc % ::influxdb/timestamp now))
                      (map influxdb/line-protocol)
                      (interpose "\n"))
                str
                metrics)
        query (http/generate-query-string {:precision "s"
                                           :org       org
                                           :bucket    bucket})
        opts  {:headers           {"Authorization"    (str "Token " apikey)
                                   "Content-Encoding" "gzip"}
               :with-credentials? false}
        url   (str host "/write?" query)]
    (a/go
      (log/debug (str "sending metrics\n" lines))
      (let [body     (a/<! (gzip lines))
            opts     (assoc opts :body body)
            response (a/<! (http/post url opts))]
        (when-not (and (:success response) (= 204 (:status response)))
          (log/warn "failed sending metrics" response))
        response))))
