(ns pp.device.io.control.digital
  (:require
    [clojure.core.async :as a]
    [integrant.core :as ig]
    [taoensso.timbre :as log]
    [pp.device.io.context :as ctx]))

(defonce ^:private five (js/require "johnny-five"))
(defonce ^:private j5-relay (.-Relay five))

(defprotocol Digital
  (state-set! [this val channel] "Sets the state of the digital to the given
  value with the given channel"))

(defn- set-relay!
  [name ^js relay on]
  (when relay
    (if on
      (.on relay)
      (.off relay))
    (log/debug (if on "relay on" "relay off") name)))

(defn- get-relay
  "Creates a stateful proxy around J5 relay. Allows backing relay to be switched
   out"
  [{:keys [context pin name]}]
  (let [ctx-chan   (ctx/get-context context)
        state      (atom {})
        relay      (atom nil)
        set-relay! #(set-relay! name @relay (some second @state))]

    (a/go-loop []
      (when-let [board (a/<! ctx-chan)]
        (let [conf {:board board
                    :pin   pin}]
          (log/info "relay becomes ready:" name)
          (reset! relay (new j5-relay (clj->js conf))))
        (recur)))

    (add-watch state ::relay.state set-relay!)
    (add-watch relay ::relay.self set-relay!)

    (reify Digital
      (state-set! [_ val channel]
        (swap! state assoc channel val)))))

(defmethod ig/init-key ::relay
  [k conf]
  (log/debug "init" k)
  (get-relay conf))
