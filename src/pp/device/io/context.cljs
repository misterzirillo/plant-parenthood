(ns pp.device.io.context
  (:require
    ["johnny-five" :refer (Board)]
    [cljs.core.async :as a]
    [integrant.core :as ig]
    [pp.device.platform :refer [arm?]]
    [taoensso.timbre :as log]))

(set! *warn-on-infer* true)

(defprotocol Context
  "IO context needed to work with hardware"
  (get-context [this] "Returns a channel that recieves an async IO context, possibly many")
  (dispose [this]))

(defonce ^:private rpi (when arm? (js/require "raspi-io")))

(def board-retry-interval-ms 30000)
(def board-default-sampling-rate 30000)

(defn- on-ready-notify [notify ^js b]
  (.on b "ready" #(do
                      (println "ready")
                      (a/put! notify b)))
  b)

(defn- try-close-board [^js board]
  (try
    (.close (.. board -io -transport))
    (catch js/Error e
      (log/error e))))

(defn- reconnect-board
  "Creates an asynchronous call chain that manages the lifecycle of a j5 context.
  If the context doesn't connect or gets disconnected a recovery will take place."
  ([details]
   (reconnect-board details :state.disconnected nil 0))
  ([{:keys [name closed newboard sampling-rate]
     :as   details}
    state
    ^js board
    retries]
   (case state

     ;; when the board is disconnected and the closed flag is false attempt a connection
     :state.disconnected
     (when-not @closed
       (let [^js board (newboard)                           ; attempt a connection
             +retries  (inc retries)
             warn      #(log/warn "IO context" name ":" (js->clj %))]
         (log/info "IO context connecting:" name)

         ;; connection failed - try again
         ;; TODO - board may report an error and still connect so this method
         ;; may create multiple boards as a result.
         #_(.once board "error"
                  (fn [_]
                    (log/warn "IO context"
                              name "failed to connect"
                              +retries "times,"
                              "retrying in" board-retry-interval-ms)
                    (js/setTimeout
                      #(reconnect-board details :state.disconnected nil +retries)
                      board-retry-interval-ms)))

         ;; setup logging
         (.on board "error" warn)
         (.on board "fail" warn)
         (.on board "warn" warn)
         (.on board "error" warn)
         (log/info "IO context" name "sampling rate:" sampling-rate)

         ;; board connects, transition to connected state
         (.once board "ready"
                (fn [_]
                  (when sampling-rate
                    (.samplingInterval board sampling-rate))
                  (log/info "IO context" name "becomes ready")
                  (reconnect-board details :state.connected board 0)))))

     ;; the board is connected, when it becomes disconnected attempt to reconnect
     :state.connected
     (do
       ;; when the flag changes manually disconnect the board
       (add-watch closed board try-close-board)

       ;; when the board disconnects attempt to reconnect
       (.once board "close"
              (fn [_]
                (log/info "IO context disconnected:" name)
                (js/setTimeout
                  #(reconnect-board details :state.disconnected nil 0)
                  board-retry-interval-ms)))))))

(defn- get-gpio-j5
  "Returns a Context wrapper for j5 GPIO (different from regular j5)"
  [config]
  (let [r-chan  (a/chan)
        r-mult  (a/mult r-chan)
        closed  (atom false)
        boardfn #(on-ready-notify
                   r-chan
                   (new Board
                        (clj->js {:io    (when rpi (new rpi.RaspiIO))
                                  :repl  false
                                  :debug false})))]
    ;; gpio is only available on ARM platforms
    (when rpi
      (reconnect-board (merge config {:newboard boardfn :closed closed})))
    (reify Context
      (get-context [_]
        (let [c (a/chan (a/sliding-buffer 1))]
          (a/tap r-mult c)
          c))
      (dispose [_]
        (reset! closed true)))))

(defn- get-external-j5
  "Returns a Context wrapper for j5"
  [{:keys [port]
    :as   config}]
  (let [r-chan  (a/chan)
        r-mult  (a/mult r-chan)
        closed  (atom nil)
        boardfn (fn []
                  (let [b (new Board
                               (clj->js {:port  port
                                         :repl  false
                                         :debug true}))]
                    (.on b "ready" #(println "ready"))
                    (on-ready-notify
                       r-chan b)))]
    (reconnect-board (merge config {:newboard boardfn :closed closed}))
    (reify Context
      (get-context [_]
        (let [c (a/chan (a/sliding-buffer 1))]
          (a/tap r-mult c)
          c))
      (dispose [_]
        (reset! closed true)))))

;; INTEGRANT

(defmethod ig/init-key ::j5-external
  [k b]
  (log/debug "init" k)
  (get-external-j5 b))

(defmethod ig/halt-key! ::j5-external
  [k board]
  (log/debug "halt" k)
  (dispose board))

(defmethod ig/init-key ::j5-gpio
  [k b]
  (log/debug "init" k)
  (get-gpio-j5 b))

(defmethod ig/halt-key! ::j5-gpio
  [k board]
  (log/debug "halt" k)
  (dispose board))
