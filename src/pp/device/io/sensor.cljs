(ns pp.device.io.sensor
  (:require
    ["johnny-five" :refer (Thermometer Sensor Multi)]
    [cljs.core.async :as a]
    [integrant.core :as ig]
    [goog.object :as goog.o]
    [pp.device.util.time :refer [now unix-seconds]]
    [pp.device.util.timeline :refer [interval]]
    [pp.device.util.influxdb :as mtr]
    [pp.device.platform :refer [arm?]]
    [pp.device.util.scheduler :refer [begin-scheduler]]
    [pp.device.io.context :as ioctx]
    [taoensso.timbre :as log]))

(defn- assoc-fields-data [data k ks metric]
  (let [x (apply goog.o/getValueByKeys data ks)]
    (if x
      (assoc-in metric [::mtr/fields k] x)
      metric)))

(defn- sensor-listen-events
  [ch stype ^js s]
  (doto s
    (.on "warn" #(log/warn stype ":" %))
    (.on "error" #(log/error stype ":" %))
    (.on "change" #(a/put! ch %))))

(defn- base-metric
  "Returns a transient map with the basic metric structure."
  [measurement]
  {::mtr/measurement measurement
   ::mtr/fields      {}
   ::mtr/tags        {}
   ::mtr/timestamp   (unix-seconds (now))})

(defn- assoc-thermo-fields
  [t mtr]
  (->> mtr
       (assoc-fields-data t :celsius ["celsius"])
       (assoc-fields-data t :fahrenheit ["fahrenheit"])))

(defn- assoc-sensor-name-tag
  [sname mtr]
  (assoc-in mtr [::mtr/tags :sensor-name] sname))

(defn- assoc-controller-name
  [cname mtr]
  (assoc-in mtr [::mtr/tags :controller] cname))

(defn- assoc-single-field
  [vname v mtr]
  (assoc-in mtr [::mtr/fields vname] v))

(defn- log-sensor-address [sname ^js s]
  (.once s "data"
         #(this-as ^js this
            (when-let [address (.-address this)]
              (log/info sname "address" (.toString address 16))))))

(defn- thermometer-sensor
  [{:keys [pin context name controller frequency
           threshold address measurement]
    :or   {frequency 5000 threshold 1}}]
  (let [ctx-chan     (ioctx/get-context context)
        metrics-chan (a/chan (a/sliding-buffer 1)
                             (map #(->> (base-metric measurement)
                                        (assoc-thermo-fields %)
                                        (assoc-sensor-name-tag name)
                                        (assoc-controller-name controller)))
                             #(log/error %))]

    (a/go-loop []
      (when-let [board (a/<! ctx-chan)]
        (let [conf   {:board      board
                      :pin        pin
                      :controller controller
                      :address    address
                      :freq       frequency
                      :threshold  threshold}
              sensor (new Thermometer (clj->js conf))]
          (log/info "thermometer becomes available:" name)
          (log-sensor-address name sensor)
          (sensor-listen-events metrics-chan "thermometer" sensor))
        (recur)))

    metrics-chan))

(defn- analog-sensor
  [{:keys [context pin name frequency measurement value threshold]
    :or   {frequency 5000 threshold 1 value "UNKNOWN"}}]
  (let [ctx-chan (ioctx/get-context context)
        mtr-chan (a/chan (a/sliding-buffer 10)
                         (map #(->> (base-metric measurement)
                                    (assoc-single-field value %)
                                    (assoc-sensor-name-tag name)))
                         #(log/error %))]

    (a/go-loop []
      (when-let [board (a/<! ctx-chan)]
        (let [conf   {:board     board
                      :pin       pin
                      :freq      frequency
                      :threshold threshold}
              sensor (new Sensor (clj->js conf))]
          (log/info "analog becomes available:" name)
          (sensor-listen-events mtr-chan "analog" sensor))
        (recur)))

    mtr-chan))

(defmulti assoc-multi-metrics identity)

(defmethod assoc-multi-metrics "BME280"
  [_ data mtr]
  (let [fields (partial assoc-fields-data data)]
    (->> mtr
         (fields :air-celcius ["thermometer" "celsius"])
         (fields :air-fahrenheit ["thermometer" "fahrenheit"])
         (fields :relative-humidity ["hygrometer" "relativeHumidity"])
         (fields :air-pressure ["barometer" "pressure"]))))

(defmethod assoc-multi-metrics "SHT31D"
  [_ data mtr]
  (let [fields (partial assoc-fields-data data)]
    (->> mtr
         (fields :air-celcius ["thermometer" "celsius"])
         (fields :air-fahrenheit ["thermometer" "fahrenheit"])
         (fields :relative-humidity ["hygrometer" "relativeHumidity"]))))

(defn- multi-sensor
  "Return a sensor object"
  [{:keys [context name controller measurement frequency threshold address]
    :or   {frequency 5000 threshold 1}}]
  (let [ctx-chan (ioctx/get-context context)
        mtr-chan (a/chan (a/sliding-buffer 10)
                         (map #(->> (base-metric measurement)
                                    (assoc-sensor-name-tag name)
                                    (assoc-controller-name controller)
                                    (assoc-multi-metrics controller %)))
                         #(log/error %))]

    ;; initialize sensor asynchronously when context becomes ready
    (a/go-loop []
      (when-let [board (a/<! ctx-chan)]
        (let [conf   {:board      board
                      :controller controller
                      :freq       frequency
                      :threshold  threshold
                      :address    address}
              sensor (new Multi (clj->js conf))]
          (log/info "multi becomes available:" name)
          (log-sensor-address name sensor)
          (sensor-listen-events mtr-chan "multi" sensor))
        (recur)))

    mtr-chan))

;; INTEGRANT

(defmethod ig/init-key ::multi
  [n conf]
  (log/debug "init" n)
  (multi-sensor conf))

(defmethod ig/halt-key! ::multi
  [_ ch]
  (a/close! ch))

(defmethod ig/init-key ::thermometer
  [k conf]
  (log/debug "init" k)
  (thermometer-sensor conf))

(defmethod ig/halt-key! ::thermometer
  [_ ch]
  (a/close! ch))

(defmethod ig/init-key ::analog
  [n conf]
  (log/debug "init" n)
  (analog-sensor conf))

(defmethod ig/halt-key! ::analog
  [_ ch]
  (a/close! ch))
