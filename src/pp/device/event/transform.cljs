(ns pp.device.event.transform
  "Defines event transforms, or objects that act as both sinks and sources
  in the pp system. Event transforms accept events like a sink, perform
  some logic (possibly stateful), then source new events as a result."
  (:require [cljs.core.async :as a]
            [cljs.core.async.impl.protocols :as ap]
            [pp.device.metrics :as p.d.metrics]
            [integrant.core :as ig]
            [taoensso.timbre :as log]))

(defn event-transform
  "Returns a psuedo-channel that can be `put!` like a normal channel
  and tapped like a mult. This is the characteristic functionality
  of any event transform."
  [xf-chan]
  (let [mult (a/mult xf-chan)]
    (reify
      ap/Channel
      (ap/close! [_]
        (ap/close! xf-chan))
      (ap/closed? [_]
        (ap/closed? xf-chan))
      ap/WritePort
      (ap/put! [_ val handler]
        (ap/put! xf-chan val handler))
      a/Mult
      (a/tap* [_ ch close?]
        (a/tap* mult ch close?))
      (a/untap* [_ ch]
        (a/untap* mult ch))
      (a/untap-all* [_]
        (a/untap-all* mult)))))

(defn- xf-threshold [pred on off]
  (comp (map pred)
        (dedupe)
        (map #(if % on off))))

(defn threshold-transform
  "Returns an event transform that emits `event-on` or `event-off`
  depending on the result of applying `predicate` to the incoming event.

  Results are 'deduped', so `event-on` will only ever be followed by
  `event-off` and vice-versa."
  [predicate event-on event-off]
  (let [ch (a/chan 1 (xf-threshold predicate event-on event-off)
                   #(log/error "threshold transformer" %))]
    (event-transform ch)))

(defn get-comparison
  [c]
  (case c
    :> >
    :< <
    :>= >=
    :<= <=
    := =
    identity))

(defmethod ig/init-key ::threshold
  [k {:keys [predicate-spec metric-key event-on event-off]}]
  (log/debug "init" k)
  (let [[pfn & args] predicate-spec
        key  (cons ::p.d.metrics/fields metric-key)
        c    (get-comparison pfn)
        pred (fn [e]
               (let [x (get-in e key)]
                 (apply c x args)))]
    (threshold-transform pred event-on event-off)))