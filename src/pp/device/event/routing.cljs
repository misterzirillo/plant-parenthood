(ns pp.device.event.routing
  (:require
   [cljs.core.async :as a]
   [integrant.core :as ig]))

(defmethod ig/init-key ::tap
  [_ tap-kvs]
  (doseq [[source sinks] tap-kvs
          sink (set sinks)]
    (a/tap source sink false)))
