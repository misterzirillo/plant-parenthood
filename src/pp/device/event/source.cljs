(ns pp.device.event.source
  (:require
    [cljs.core.async :as a]
    [integrant.core :as ig]
    [taoensso.timbre :as log]
    [pp.device.metrics :refer [validate-metric]]
    [pp.device.util.scheduler :refer [begin-scheduler]]
    [pp.device.util.timeline :as timeline]
    [pp.device.event :as event]
    [pp.device.util.time :as time]))

;; METRICS STUFF
(defn- sensor-metrics
  [sensor]
  (let [xf  (filter validate-metric)
        out (a/chan (a/sliding-buffer 10) xf)]
    (a/pipe sensor out false)
    out))

(defmethod ig/init-key ::metrics.sensors
  [k [& sensorss]]
  (log/debug "init" k)
  (let [sensors (apply concat sensorss)
        chs     (mapv sensor-metrics sensors)]
    {::mult  (-> chs a/merge a/mult)
     ::chans chs}))

(defmethod ig/resolve-key ::metrics.sensors
  [_ {::keys [mult]}]
  mult)

(defmethod ig/halt-key! ::metrics.sensors
  [k {::keys [chans]}]
  (log/debug "halt" k)
  (doseq [ch chans]
    (a/close! ch)))

;; SCHEDULER STUFF
(defn- last-event
  [timeline]
  (let [now (time/now)]
    (->> timeline
         (map #(.subtract ^js % 1 "day"))
         (take-while #(.isBefore ^js % now))
         last)))

(defn- initial-state
  "Return a set of commands that were executed last"
  [command-schedule]
  (->> command-schedule
       ;; associate each command with the last time it was executed
       (map (fn [{:keys [timeline command]}]
              (let [tl         (timeline/hydrate-timeline timeline)
                    last-event (last-event tl)]
                [command last-event])))
       ;; group previous commands by which target they affect
       (group-by (comp ::event/command.target first))
       ;; only keep the last command for any given target
       (map
         (fn [[_ group]]
           (->> group
                (sort-by (comp time/unix-ms second))
                last
                first)))
       set))

(defn- schedule-command
  [name command tl-spec replay?]
  (let [c        (a/chan (a/dropping-buffer 1)
                         (comp (map (constantly command))
                               (map #(assoc %
                                       ::event/timestamp (time/now)
                                       ::event/command.channel name))))
        timeline (timeline/hydrate-timeline tl-spec)]
    (when replay?
      ;; the command should be executed right away
      (a/put! c command))
    (begin-scheduler timeline {:ch c})))

(defmethod ig/init-key ::commands.schedule
  [k {:keys [command-schedule]}]
  (log/debug "init" k)
  (let [replay (initial-state command-schedule)
        chs    (mapv (fn [{:keys [command timeline name]}]
                       (log/info "scheduling" name)
                       (schedule-command name command timeline (replay command)))
                     command-schedule)
        mchan  (a/merge chs (count chs))]
    (log/info "replaying command state" replay)
    {::mult  (a/mult mchan)
     ::chans chs}))

(defmethod ig/resolve-key ::commands.schedule
  [_ {::keys [mult]}]
  mult)

(defmethod ig/halt-key! ::commands.schedule
  [k {::keys [chans]}]
  (log/debug "halt" k)
  (doseq [c chans]
    (a/close! c)))
