(ns pp.device.event.sink
  (:require
    [pp.device.io.control.digital :as ctl.digital]
    [pp.device.util.scheduler :as sched]
    [pp.device.util.timeline :as timeline]
    [pp.device.metrics :as mtr]
    [pp.device.event :as evt]
    [cljs.core.async :as a]
    [integrant.core :as ig]
    [taoensso.timbre :as log]))

(defmethod ig/init-key ::commands.relay
  [k relay-kvs]
  (log/debug "init" k)
  (let [in     (a/chan)
        relays (into {} relay-kvs)]
    (a/go-loop []
      (let [{target ::evt/command.target
             value  ::evt/command.value
             channel ::evt/command.channel
             :as    cmd} (a/<! in)
            relay (get relays target)]
        (when (and relay cmd)
          (try
            (ctl.digital/state-set! relay value channel)
            (catch js/Error e
              (log/error e)))
          (recur))))
    in))

(defmethod ig/halt-key! ::commands.relay
  [k chan]
  (log/debug "halt" k)
  (a/close! chan))

(defmethod ig/init-key ::metrics.upload
  [k {:keys [period influxdb]
      :or   {period {:minute 1}}}]
  (log/debug k)
  (log/info "uploading metrics every" period)
  (let [sched       (sched/begin-scheduler (timeline/interval period))
        metrics-in  (a/chan 10 (filter mtr/validate-metric))
        metrics-buf (atom [])]

    ;; periodically send the metrics according to scheduler
    (a/go-loop []
      (when (a/<! sched)
        (when-let [data (not-empty @metrics-buf)]
          (reset! metrics-buf [])
          (mtr/post-metrics influxdb data))
        (recur)))

    ;; take in metrics when they are available
    (a/go-loop []
      (when-let [m (a/<! metrics-in)]
        (swap! metrics-buf conj m)
        (recur)))

    {:sink metrics-in
     :chs  [metrics-in sched]}))

(defmethod ig/resolve-key ::metrics.upload
  [_ {:keys [sink]}]
  sink)

(defmethod ig/halt-key! ::metrics.upload
  [k {:keys [chs]}]
  (log/debug "halt" k)
  (doseq [ch chs]
    (a/close! ch)))

(defmethod ig/init-key ::logs.debug
  [_ debug-str]
  (let [in (a/chan 10)]
    (a/go-loop []
      (when-let [printme (a/<! in)]
        (log/debug debug-str "--" printme)
        (recur)))
    in))

(defmethod ig/halt-key! ::logs.debug
  [_ ch]
  (a/close! ch))
