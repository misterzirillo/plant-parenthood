(ns pp.device.util.timeline
  (:require
    [cljs.spec.alpha :as s]
    [pp.device.util.time :as t]))

(s/def ::type #{::type.interval ::type.static})
(s/def ::basis ::t/temporal)
(s/def ::offset ::t/temporal)
(s/def ::before ::t/temporal)
(s/def ::after ::t/temporal)

(s/def ::timeline
  (s/keys :req [::type ::basis]
          :opt [::offset ::before ::after]))

(defn interval
  ([t] (interval t nil nil))
  ([t start] (interval t start nil))
  ([t start end]
   (let [start (or start (t/now))
         i     (iterate (partial t/adjust-temporal t) start)]
     (if end
       (take-while (partial t/after? end) i)
       i))))

(defn- repeat-daily
  "Repeats the first day of the timeline"
  [timeline]
  (let [cutoff   (->> timeline first (t/adjust-temporal {:day 1}))
        timeline (vec (take-while (partial t/after? cutoff) timeline))]
    (eduction
      (map-indexed
        (fn [i times]
          (map (partial t/adjust-temporal {:day i}) times)))
      (mapcat identity)
      (repeat timeline))))

(defmulti hydrate-basis identity)

(defmethod hydrate-basis ::type.interval
  [_ {::keys [basis offset before after]}]
  (let [today    (t/today)
        interval (->> (interval basis today)
                      (map (partial t/adjust-temporal offset)))
        cutoff   (->> interval first (t/adjust-temporal {:day 1}))
        before   (if before (t/adjust-temporal before today) (t/tomorrow))
        after    (if after (t/adjust-temporal after today) (t/yesterday))]
    (->> interval
         (take-while (partial t/after? cutoff))
         (map #(.set % "date" (.get today "date")))
         (filter (fn [d]
                   (let [invert  (t/before? before after)
                         between (t/between? before after d invert)]
                     (if invert (not between) between))))
         (sort t/difference-ms)
         repeat-daily)))

(defmethod hydrate-basis ::type.static
  [_ {::keys [basis offset]}]
  (->> (t/today)
       (t/adjust-temporal basis)
       (interval {:day 1})
       (map (partial t/adjust-temporal offset))))

(defn hydrate-timeline [spec]
  (if (s/valid? ::timeline spec)
    (hydrate-basis (::type spec) spec)
    (throw (new js/Error
                (ex-info "invalid timeline"
                         (s/explain-data ::timeline spec))))))

(comment
  (->> (hydrate-timeline
         {::type   ::type.interval
          ::basis  {:hour 3.5}
          ::offset {:hour   21
                    :second 10}
          ::before {:hour 9}
          ::after  {:hour 21}
          })
       (take 8)
       (map (partial t/adjust-temporal {:hour -6}))))