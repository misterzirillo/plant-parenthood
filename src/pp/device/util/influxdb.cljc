(ns pp.device.util.influxdb
  "Defines a spec for the InfluxDB line protocol as well as transforms to raw
  line protocol strings.

  This namespace follows the line protocol specification found here:
  https://docs.influxdata.com/influxdb/v2.0/reference/syntax/line-protocol"
  (:require [clojure.spec.alpha :as s]))

(defn- not-starts-with-_
  "Determine if the given string starts with an underscore character."
  [s]
  (-> s
      (name)
      (clojure.string/starts-with? "_")
      (not)))

(defn- <64KB
  "Determine if the given string is less than (exclusive) 64000 bytes per
  [[bytes]]."
  [s]
  (> 64000
     (-> s (name) (bytes) (count))))

;(defn- with-timestamp-bounds
;  "Determine if the given number is within InfluxDB timestamp bounds."
;  [ts]
;  (and (>= ts -9223372036854775806)
;       (<= ts 9223372036854775806)))

;; https://docs.influxdata.com/influxdb/v2.0/reference/syntax/line-protocol/#string
(s/def ::string
  (s/and string? <64KB))

(s/def ::key
  (s/or :string (s/and ::string not-starts-with-_)
        :keyword (s/and keyword? <64KB not-starts-with-_)))

(s/def ::measurement ::key)

(s/def ::field-value
  (s/or :string ::string
        :boolean boolean?
        :integer int?
        :float float?))

(s/def ::fields
  (s/and not-empty
         (s/map-of ::key ::field-value)))

(s/def ::tags (s/map-of ::key ::string))

;; https://docs.influxdata.com/influxdb/v2.0/reference/syntax/line-protocol/#unix-timestamp
(s/def ::timestamp
  int?)

(s/def ::point
  (s/keys :req [::measurement ::fields ::timestamp]
          :opt [::tags]))

(defn- key=val
  "Given key-value vector return a string <key>=<value>"
  [[k v]]
  (str k "=" v))

(defn- escape* [c]
  (fn [s]
    (clojure.string/replace s c (str \\ c))))

;; https://v2.docs.influxdata.com/v2.0/reference/syntax/line-protocol/#special-characters
(def ^:private replace-dbl-quote (escape* \"))
(def ^:private replace-equals (escape* \=))
(def ^:private replace-comma (escape* \,))
(def ^:private replace-backslash (escape* \\))
(def ^:private replace-space (escape* " "))

(def ^:private escape-field-string-value
  (comp replace-dbl-quote
        replace-backslash
        name))

(def ^:private escape-tag-kv
  (comp replace-comma
        replace-equals
        replace-space
        name))

(def ^:private escape-field-key
  (comp replace-comma
        replace-equals
        replace-space
        name))

(def ^:private escape-measurement
  (comp replace-comma
        replace-space
        name))

(defn- quote-field-string-val [s]
  (if (string? s)
    (str \" (escape-field-string-value s) \")
    s))

(def ^:private xf-map-tags
  (map (juxt (comp escape-tag-kv first)
             (comp escape-tag-kv second))))

(def ^:private xf-map-fields
  (map (juxt (comp escape-field-key first)
             (comp quote-field-string-val second))))

(defn- munge-kv-pairs [xf kvs]
  (transduce (comp xf (distinct) (map key=val) (interpose \,))
             str kvs))

(defn line-protocol
  "Takes a point and encodes it according to InfluxDB line protocol."
  [{::keys [measurement timestamp fields tags]}]
  (str (escape-measurement measurement)
       (if (not-empty tags) \, "")
       (munge-kv-pairs xf-map-tags tags)
       " "
       (munge-kv-pairs xf-map-fields fields)
       " "
       timestamp))