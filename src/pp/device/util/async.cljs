(ns pp.device.util.async
  (:require
   [clojure.core.async :as a]))

(defn to-promise
  [chan]
  (let [p (a/promise-chan)]
    (a/go
      (->> chan
           a/<!
           (a/>! p)))
    p))
