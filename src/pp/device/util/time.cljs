(ns pp.device.util.time
  (:require
    [clojure.spec.alpha :as s]
    ["dayjs" :as dayjs]
    ["dayjs/plugin/relativeTime" :as relativeTime]
    ["dayjs/plugin/customParseFormat" :as customParseFormat]
    ["dayjs/plugin/isBetween" :as isBetween]))

(set! *warn-on-infer* false)

(.extend dayjs relativeTime)
(.extend dayjs customParseFormat)
(.extend dayjs isBetween)

;; spec
(defn valid-min-or-sec? [t]
  (<= (max t (- t))
      60))

(defn valid-hour? [t]
  (<= (max t (- t))
      24))

(s/def ::second
  (s/and number? valid-min-or-sec?))

(s/def ::minute
  (s/and number? valid-min-or-sec?))

(s/def ::hour
  (s/and number? valid-hour?))

(s/def ::temporal
  (s/keys
    :req-un [(or ::second ::minute ::hour)]))

(defn now []
  (dayjs))

(defn unix-seconds [^js d]
  (.unix d))

(defn unix-ms [^js d]
  (.valueOf d))

(defn difference-ms
  [d1 d2]
  (- (unix-ms d1)
     (unix-ms d2)))

(defn is-valid? [d]
  (try
    (.isValid d)
    (catch js/Error _
      false)))

(defn before? [^js d1 ^js d2]
  (.isBefore d1 d2))

(defn after? [^js d1 ^js d2]
  (.isAfter d1 d2))

(defn between? [^js before ^js after ^js d inclusive]
  (let [i (if inclusive "()" "[]")]
    (.isBetween d before after nil i)))

(defn reset-temporal [^js t temporal]
  (reduce
    (fn [^js t [k v]]
      (.set t (name k) v))
    t
    temporal))

(defn today []
  (reset-temporal (now)
                  {:hour        0
                   :minute      0
                   :second      0
                   :millisecond 0}))

(defn adjust-temporal
  "Adds the given duration to t"
  [{:keys [second hour minute day]
    :or   {day    0
           second 0
           hour   0
           minute 0}}
   t]
  (-> t
      (.add second "second")
      (.add minute "minute")
      (.add hour "hour")
      (.add day "day")))

(defn yesterday []
  (->> (today)
       (adjust-temporal {:day -1})))

(defn tomorrow []
  (->> (today)
       (adjust-temporal {:day 1})))
