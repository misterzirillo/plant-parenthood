(ns pp.device.util.scheduler
  (:require
    [cljs.core.async :as a]
    [cljs.core.async.impl.protocols :as p]
    [pp.device.util.time :as d]))

(defn begin-scheduler
  [timeline & [{:keys [ch] :or {ch (a/chan)}}]]
  ;; borrowed respectfully from https://github.com/jarohen/chime
  ;; which doesn't support cljs
  (let [cancel-ch (a/chan)]
    (a/go-loop [now       (d/now)
                times-seq (->> timeline
                               (drop-while #(.isBefore ^js % now)))]
      (if-let [[next-time & more-times] (seq times-seq)]
        (a/alt!
          cancel-ch (a/close! ch)
          (a/timeout
            (d/difference-ms next-time now)) (do (a/put! ch next-time)
                                                 (recur (d/now) more-times))
          :priority true)
        (a/close! ch)))

    (reify
      p/ReadPort
      (take! [_ handler]
        (p/take! ch handler))
      p/Channel
      (close! [_]
        (p/close! cancel-ch))
      (closed? [_]
        (p/closed? cancel-ch)))))
