(ns pp.device.core
  (:require
   [aero.core :as aero]
   [pp.device.io.context]
   [pp.device.io.control.digital]
   [pp.device.io.sensor]
   [pp.device.event.sink]
   [pp.device.event.source]
   [pp.device.event.routing]
   [pp.device.event.transform]
   [pp.device.db]
   [integrant.core :as ig]
   [taoensso.timbre :as log]))

(defmethod aero/reader 'ig/ref
  [_ _ value]
  (ig/ref value))

(defmethod aero/reader 'ig/refset
  [_ _ value]
  (ig/refset value))

(defmethod aero/reader 'ig/refk
  [_ _ value]
  [value (ig/ref value)])

(defn external-config
  "Parse the given aero configuration"
  ([profile] (external-config profile "config.edn"))
  ([profile filename]
   (try
     (log/info "reading config" filename)
     (aero/read-config filename {:profile profile})
     (catch js/Error e
       (log/error e)
       (log/warn "failed to load configuration" filename)
       {}))))

(defonce system (atom nil))

(defn init-system!
  "Starts the various components that compose this application."
  [profile config-filename]
  (try
    (let [p (or profile :dev)
          c (:system (external-config p config-filename))]
      (log/info "initializing system with profile" p)
      (reset! system (ig/init c)))
    (catch js/Error e
      (log/error e))))

(defn halt-system!
  "Halts the application"
  []
  (log/info "halting system")
  (try
    (ig/halt! @system)
    (reset! system nil)
    (catch js/Error e
      (log/info e)))
  (log/info "system halted"))

(defn reset-system! [profile]
  (log/info "suspending system..."
            (ig/suspend! @system))
  (log/info "resuming system..."
            (ig/resume (-> profile external-config :system)
                       @system)))

(defn -main
  [config & _]
  (log/set-level! :info)
  (init-system! :prod config))
