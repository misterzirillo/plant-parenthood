(ns pp.device.platform
  (:require [taoensso.timbre :as log]))

(def arm?
  "Is this runtime on an IOT device?"
  (and js/process (= "arm" js/process.arch)))

(when-not arm?
  (log/warn "Platform is not ARM - some IO will be unavailable"))
