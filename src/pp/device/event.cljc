(ns pp.device.event
  (:require
    [pp.device.util.time :as time]
    [cljs.spec.alpha :as s]))

(s/def ::timestamp time/is-valid?)
(s/def ::event
  (s/keys :req [::timestamp]))

(s/def ::command.target any?)
(s/def ::command.value any?)
(s/def ::command.channel any?)

(s/def ::command
  (s/and ::event
         (s/keys :req [::command.target
                       ::command.value
                       ::command.channel])))

(comment
  (s/explain-str ::event {::timestamp (time/now)})
  (s/explain-str ::command {::command.target  'a.target
                            ::command.value   'a.value
                            ::command.channel 'a.channel
                            ::timestamp       (time/now)}))
