(ns cljs.user
  (:require
   [pp.device.core :refer [init-system! halt-system! reset-system!]]))

(enable-console-print!)

(defn go
  ([] (init-system! :dev "config.edn"))
  ([config]
   (init-system! :dev config)
   :ok))

(defn halt []
  (halt-system!))

(defn reset []
  (reset-system! :dev))
